require 'test_helper'

class PrimesCheckTest < ActionDispatch::IntegrationTest
  test "EratosthenesSieve state is persist between calls" do
    def assert_state
      assert_equal 1574, Prime::EratosthenesSieve.instance.instance_variable_get("@max_checked")
    end

    get "/primes/999331"
    assert_equal({ prime: true }.to_json, response.body)
    assert_state
    
    get "/primes/12"
    assert_equal({ prime: false }.to_json, response.body)
    assert_state
  end
end
