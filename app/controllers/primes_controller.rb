class PrimesController < ApplicationController
  def show
    result = Prime.prime?(params[:id].to_i, Prime::EratosthenesGenerator.new)
    render json: { prime: result }
  end
end
